<?php
require_once 'core/functions.php';

$blog = getBlogById($_GET['s']);
//$img1 = '/uploads/id1_bmw-erc-pole-24h-motos.jpg';
//$img2 = '/uploads/id2_BMW du ERC-BMW Motorrad.jpg';
//$img3 = '/uploads/id3_yamaha_mt07.jpg';
//$img4 = '/uploads/id4_yam-motoroid-hd.jpg';
//$img5 = '/uploads/id5_Yamaha-MT-07-EU-Race-Blu-Studio.jpg';

if($blog === false) {
    header('Location: /404.php');
}

getHeader($blog->title);
?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h1 class="text-center">
                    <?php echo $blog->title;?>
                </h1>
            </div>

            <div class="col-12 text-center">
                <?php
                if (!empty($blog->createdAt)) {
                    $date = new DateTime($blog->createdAt);
                    echo "Article publié le " . $date->format('d/m/y H:i');
                }
                ?>
            </div>

            <div class="col-8 text-center mt-3">
                <?php echo "";?>
            </div>

            <div class="col-12 mt-5">
                <?php echo "$blog->id)" ;?>
                <?php echo $blog->content;?>
            </div>

        </div>
    </div>
<?php
getFooter();