<?php
session_start();
require_once $_SERVER['DOCUMENT_ROOT'] .'/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/config.php';


function getHeader(string $title)
{
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';
}

function getFooter()
{
    include $_SERVER['DOCUMENT_ROOT'] .'/partials/footer.php';
}

/**
 * @return PDO|null
 */
function getDatabaseConnexion(): ?PDO
{
    try {
        $host = 'mysql:host=localhost;dbname='. DB_NAME. ';charset=utf8';
        $db = new PDO($host, DB_USER, DB_PASSWD);
        return $db;
    } catch (Exception $e) {
        return null;
    }
}

/**
 * @param int $limit
 * @return array
 */
function getLastBlog(int $limit = 3): array{
    if(null === $db = getDatabaseConnexion()) {
        return [];
    }
    $response = $db->query("SELECT * FROM blog ORDER BY title DESC LIMIT $limit");
    return $response->fetchAll(PDO::FETCH_OBJ);
}

/**
 * @param string $id
 * @return mixed|null
 */
function getBlogById(int $id)
{
    if(null === $bd = getDatabaseConnexion()) {
        return null;
    }
    $response = $bd->query("SELECT * FROM blog WHERE id = '$id' LIMIT 1");

    return $response->fetchObject();
}