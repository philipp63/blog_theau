<?php
require_once 'core/functions.php';
$title = "Blog moto";
getHeader($title);

$lastBlogs = getLastBlog(3);

?>

    <div class="container accueil" >
        <div class="row">
            <div class="col-md-6">
                <div id="carouselMotos" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="/uploads/bmw-erc-pole-24h-motos_id1.jpg" class="d-block w-100" alt="BMW pôle positioin 24h motos.">
                        </div>
                        <div class="carousel-item">
                            <img src="/uploads/yam-motoroid-hd_id1.jpg" class="d-block w-100" alt="Moto robot.">
                        </div>
                        <div class="carousel-item">
                            <img src="/uploads/yamaha_mt07-2_id3.jpg" class="d-block w-100" alt="Meilleure vente de cette année.">
                        </div>
                        <a class="carousel-control-prev" href="#carouselMotos" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Précédente</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselMotos" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Suivante</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <h1>Bienvenue sur votre blog</h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur dignissimos dolore nam. Aliquid beatae blanditiis, doloremque ipsa ipsum iure, mollitia necessitatibus nihil obcaecati officiis pariatur perferendis quod reprehenderit soluta voluptate? Accusamus accusantium animi asperiores commodi consequatur cum cumque delectus dolor dolore doloribus dolorum error esse eum fugiat hic illo illum inventore ipsa iste magni maxime molestias natus, nobis nulla numquam porro quas quasi quia quos ratione, recusandae repellat sint sit soluta sunt unde voluptate! A alias architecto blanditiis est et laboriosam magnam quibusdam repudiandae vero voluptatum. Alias, aliquam doloribus esse eum harum inventore ipsam magnam molestiae, sit tempora, unde veritatis!
                </p>
            </div>


            <div class="col-12 mt-3">
                <h2 class="text-center">Derniers articles parus</h2>
            </div>
            <?php foreach ($lastBlogs as $blog):?>
                <div class="col-lg-4">
                    <?php include 'partials/cards/blog-card.php'; ?>
                </div>
            <?php endforeach;?>
        </div>
    </div>

<?php
getFooter();

//G:\wamp64\www\blog_Theau>git push -u origin master
//fatal: unable to access 'https://gitlab.ocm/philipp63/blog_theau.git/': Could not resolve host: gitlab.ocm

?>
