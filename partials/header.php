<?php
    if(!isset($title)) {
        $title = "Blog Perso";
    }
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/build/style.css">
    <title><?php echo $title; ?></title>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-light md-light">
        <div class="container">
            <button type="button" class="btn btn-primary">Le Blog de la Moto</button>
            <input type="button" class="favorite styled" value="Articles">
            <button type="button" class="btn btn-default">Contact</button>
        </div>
    </nav>
</header>