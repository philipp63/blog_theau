<?php if (isset($blog)): ?>
    <?php

    $imgUrl = '/uploads/650 XTZ_pointe des Roches.jpg';

    if(!empty($blog->image)) {
        $imgUrl = '/uploads/' . $blog->image;
    }
    ?>
    <div class="card">
        <img src="<?php echo $imgUrl; ?>" class="card-img-top"
             alt="...">
        <div class="card-body">
            <h5 class="card-title">
                <?php echo $blog->title; ?>
            </h5>
<!--            --><?php //if (!empty($blog->id)): ?>
<!--                <p class="card-text">-->
<!--                    --><?php //echo $blog->excerpt; ?>
<!--                </p>-->
<!--            --><?php //endif; ?>

            <a href="/blog.php?s=<?php echo $blog->id; ?>" class="btn btn-primary">Lire plus</a>
        </div>
    </div>
<?php else:
    echo 'Problème article';
endif;